#pragma once

#include <Arduino.h>
#include "HID.h"
#include "HID-Settings.h"
#include "../HID-APIs/WheelAPI.h"

class Wheel_ : public WheelAPI
{
public:
	Wheel_(void);

protected:
	virtual inline void SendReport(void* data, int length) override;
};