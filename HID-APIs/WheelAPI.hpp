#pragma once

WheelAPI::WheelAPI(void)
{

}

void WheelAPI::Begin(void)
{
	//release all buttons
	End();
}

void WheelAPI::End(void)
{
	memset(&_report, 0x00, sizeof(_report));
	SendReport(&_report, sizeof(_report));
}

void WheelAPI::Write(void)
{
	SendReport(&_report, sizeof(_report));
}

void WheelAPI::Press(uint8_t button)
{
	_report.buttons |= (uint32_t)1 << (button - 1);
}

void WheelAPI::Release(uint8_t button)
{
	_report.buttons &= ((uint32_t)1 << (button - 1));
}

void WheelAPI::ReleaseAll(void)
{
	memset(&_report, 0x00, sizeof(_report));
}

void WheelAPI::Buttons(uint32_t buttons)
{
	_report.buttons = buttons;
}

void WheelAPI::XAxis(int16 axis)
{
	_report.xAxis = axis;
}

void WheelAPI::YAxis(int16_t axis)
{
	_report.yAxis = axis;
}

void WheelAPI::RXAxis(int16_t axis)
{
	_report.rxAxis = axis;
}

void void WheelAPI::RYAxis(int16_t axis)
{
	_report.ryAxis = axis;
}

void WheelAPI::ZAxis(int8_t axis)
{
	_report.zAxis = axis;
}

void WheelAPI::RZAxis(int8_t axis)
{
	_report.rzAxis = axis;
}

void WheelAPI::DPad1(int8_t dPad)
{
	_report.dPad1 = dPad;
}

void WheelAPI::DPad2(int8_t dPad)
{
	_report.dPad2 = dPad;
}