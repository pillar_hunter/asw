#pragma once

#include <Arduino.h>
#include "HID-Settings.h"

typedef union ATTRIBUTE_PACKED
{
	uint8_t whole8[0];
	uint16_t whole16[0];
	uint32_t whole32[0];
	uint32_t buttons;

	struct ATTRIBUTE_PACKED
	{
		uint8_t button1 : 1;
		uint8_t button2 : 1;
		uint8_t button3 : 1;
		uint8_t button4 : 1;
		uint8_t button5 : 1;
		uint8_t button6 : 1;
		uint8_t button7 : 1;
		uint8_t button8 : 1;
		uint8_t button9 : 1;
		uint8_t button10 : 1;
		uint8_t button11 : 1;
		uint8_t button12 : 1;
		uint8_t button13 : 1;
		uint8_t button14 : 1;
		uint8_t button15 : 1;
		uint8_t button16 : 1;
		uint8_t button17 : 1;
		uint8_t button18 : 1;
		uint8_t button19 : 1;
		uint8_t button20 : 1;
		uint8_t button21 : 1;
		uint8_t button22 : 1;
		uint8_t button23 : 1;
		uint8_t button24 : 1;
		uint8_t button25 : 1;
		uint8_t button26 : 1;
		uint8_t button27 : 1;
		uint8_t button28 : 1;
		uint8_t button29 : 1;
		uint8_t button30 : 1;
		uint8_t button31 : 1;
		uint8_t button32 : 1;

		int16_t xAxis;
		int16_t yAxis;
		int16_t rxAxis;
		int16_t ryAxis;
		int8_t zAxis;
		int8_t rzAxis;

		uint8_t dPad1 : 4;
		uint8_t dPad2 : 4;
	};
} HID_WheelReport_Data_t;

class WheelAPI
{
public:
	inline WheelAPI(void);
	inline void Begin(void);
	inline void End(void);
	inline void Press(uint8_t button);
	inline void Release(uint8_t button);
	inline void ReleaseAll(void);
	inline void Buttons(uint32_t buttons);
	inline void XAxis(int16_t axis);
	inline void YAxis(int16_t axis);
	inline void RXAxis(int16_t axis);
	inline void RYAxis(int16_t axis);
	inline void ZAxis(int8_t axis);
	inline void RZAxis(int8_t axis);
	inline void DPad1(int8_t dPad);
	inline void DPad2(int8_t dPad);

	virtual void SendReport(void* data, int length) = 0;

protected:
	HID_WheelReport_Data_t _report;
};

#include "WheelAPI.hpp"