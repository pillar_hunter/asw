#pragma once

#if ARDUINO < 10607
#error Project requires Arduino IDE v1.6.7 or greater.
#endif

#ifndef(USBCON)
#error Project can only be used with an USB MCU.
#endif

#include "SingleReport/SingleWheel.h"
#include "MultiReport/Wheel.h"