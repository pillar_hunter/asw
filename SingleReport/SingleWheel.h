#pragma once

#include <Arduino.h>
#include "HID.h"
#include "HID-Settings.h"
#include "../HID-APIs/WheelAPI.h"

class SingleWheel_ : public PluggableUSBModule, public WheelAPI
{
public:
	SingleWheel_(void);

protected:
	int GetInterface(uint8_t* interfaceCount);
	int GetDescriptor(USBSetup& setup);
	bool Setup(USBSetup& setup);
	uint32_t epType[1];
	uint8_t protocol;
	uint8_t idle;

	virtual void SendReport(void* data, int length) override;
};